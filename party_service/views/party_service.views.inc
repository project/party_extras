<?php
/**
 * @file
 * Provides integrations with the Views module.
 */

/**
 * Implements hook_views_plugins().
 */
function party_service_views_plugins() {
  $plugins = array();
  $plugins['argument validator']['party_service_machine_name'] = array(
    'title' => t('Party Service Machine Name'),
    'handler' => 'party_service_plugin_argument_validate_party_service_machine_name',
  );
  return $plugins;
}
