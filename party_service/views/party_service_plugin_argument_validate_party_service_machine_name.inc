<?php
/**
 * @file
 * Contains plugin that makes sure the machine name of a party_Service entity is used.
 */

class party_service_plugin_argument_validate_party_service_machine_name extends views_plugin_argument_validate {

  function validate_argument($argument) {
    if (is_object($argument)) {
      if (!empty($argument->name)) {
        $this->argument->argument = $argument->name;
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }
}
