<?php
/**
 * @file
 * Main module code for Party Services.
 */

define('PARTY_SERVICE_BAD_REQUEST', 400);
define('PARTY_SERVICE_METHOD_NOT_ALLOWED', 405);

/**
 * Implements hook_hook_info().
 */
function party_service_hook_info() {
  // Default Config.
  $hooks['default_party_service'] = array('group' => 'party_service_default');
  $hooks['default_party_service_alter'] = array('group' => 'party_service_default');
  $hooks['default_party_service_remote'] = array('group' => 'party_remote_default');
  $hooks['default_party_service_remote_alter'] = array('group' => 'party_remote_default');

  // Party Service Hooks
  $hooks['party_service_post'] = array('group' => 'party_service');

  return $hooks;
}

/**
 * Implements hook_entity_info().
 */
function party_service_entity_info() {
  $info = array();

  $info['party_service'] = array(
    'label' => t('Party Web Service'),
    'base table' => 'party_service',
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIControllerExportable',
    'fieldable' => FALSE,
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'name',
      'label' => 'label',
    ),
    'module' => 'party_service',
    'access callback' => 'party_service_access',
    'admin ui' => array(
      'path' => 'admin/config/services/party_services',
      'file' => 'party_service.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),
  );

  $info['party_service_key'] = array(
    'label' => t('Party Service API Key'),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'party_service_keys',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'apikey',
      'status' => 'export_status',
    ),
    // Just have on bundle with the same name as the entity
    'bundles' => array(
      'party_service_key' => array(
        'label' => 'Party Service API Key',
      ),
    ),
    'access callback' => 'party_service_key_access',
    'module' => 'party_service',
    'admin ui' => array(
      'path' => 'admin/config/services/party_services/manage/%party_service/keys',
      'file' => 'carkit_key.admin.inc',
      'controller class' => 'PartyServiceKeyUIController',
      'menu wildcard' => '%party_service_key',
    ),
  );

  $info['party_service_remote'] = array(
    'label' => t('Party Web Remote'),
    'base table' => 'party_service_remote',
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIControllerExportable',
    'fieldable' => FALSE,
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'name',
      'label' => 'label',
    ),
    'module' => 'party_service',
    'access callback' => 'party_service_remote_access',
    'admin ui' => array(
      'path' => 'admin/config/services/party_remote',
      'file' => 'party_service_remote.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),
  );

  return $info;
}

/**
 * Implements hook_entity_info_alter().
 */
function party_service_entity_info_alter(&$info) {
  $info['party']['uuid'] = TRUE;
  $info['party']['entity keys']['uuid'] = 'uuid';
}

/**
 * Implements hook_menu().
 */
function party_service_menu() {
  $items = array();
  $items['party_service/%party_service'] = array(
    'title' => 'Party Service',
    'page callback' => 'party_service_callback',
    'page arguments' => array(1),
    'access callback' => 'party_service_callback_access',
    'access arguments' => array(1),
    'delivery callback' => 'party_service_deliver',
    'file' => 'party_service.service.inc',
  );
  $items['party_service/%party_service/%party_uuid'] = array(
    'title' => 'Party Service',
    'page callback' => 'party_service_callback',
    'page arguments' => array(1,2),
    'access callback' => 'party_service_callback_access',
    'access arguments' => array(1,2),
    'delivery callback' => 'party_service_deliver',
    'file' => 'party_service.service.inc',
  );

  return $items;
}

/**
 * Implements hook_views_api().
 */
function party_service_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'party_service') . '/views',
  );
}

/**
 * Implements hook_permission().
 */
function party_service_permission() {
  $perms = array();
  $perms['configure party services'] = array(
    'title' => t('Configure Party Services'),
  );
  $perms['configure party service remotes'] = array(
    'title' => t('Configure Party Service Remotes'),
  );

  return $perms;
}

/**
 * Load a party by its uuid.
 */
function party_uuid_load($uuid) {
  $entities = entity_uuid_load('party', array($uuid));
  return reset($entities);
}

/**
 * Load a party service entity.
 */
function party_service_load($id) {
  return entity_load_single('party_service', $id);
}

/**
 * Load a party service entity.
 */
function party_service_remote_load($id) {
  return entity_load_single('party_service_remote', $id);
}

/**
 * Access callback for party services.
 */
function party_service_access($op, $service = NULL, $account = NULL) {
  return user_access('configure party services', $account);
}

/**
 * Load a party service key.
 */
function party_service_key_load($id) {
  return entity_load_single('party_service_key', $id);
}

/**
 * Access callback for party service keys.
 */
function party_service_key_access($op, $key = NULL, $account = NULL) {
  return user_access('configure party services', $account);
}

/**
 * Access callback for party service remotes.
 */
function party_service_remote_access($op, $remote = NULL, $account = NULL) {
  return user_access('configure party service remotes', $account);
}

/**
 * Service Access
 */
function party_service_callback_access($service, $party = NULL) {
  // For a version just check that they have access using the checks with the
  // API Key.
  if (empty($_SERVER['HTTP_X_API_KEY'])) {
    return FALSE;
  }

  $api_key = $_SERVER['HTTP_X_API_KEY'];
  $key = entity_load_single('party_service_key', $api_key);

  // Check the key exists for this service.
  if (empty($key) || empty($key->status) || $key->service != $service->name) {
    return FALSE;
  }

  // Check the key is allowed to used from the domain.
  // @todo: This doesn't work and I can't see an easy way of making it work
  //if (!empty($key->from_domain) && $key->from_domain != $_SERVER['HTTP_ORIGIN']) {
  //  return FALSE;
  //}

  return TRUE;
}

/**
 * Service Delivery Callback.
 */
function party_service_deliver($page_callback_result) {
  switch ($page_callback_result) {
    case MENU_NOT_FOUND:
    case MENU_SITE_OFFLINE:
      drupal_add_http_header('status', 404);
      break;
    case MENU_ACCESS_DENIED:
      drupal_add_http_header('status', 403);
      break;
    case PARTY_SERVICE_METHOD_NOT_ALLOWED:
      drupal_add_http_header('status', 405);
      break;
    case PARTY_SERVICE_BAD_REQUEST:
      drupal_add_http_header('status', 400);
      break;
    default:
      if (is_string($page_callback_result)) {
        print $page_callback_result;
      }
      else {
        if (!empty($page_callback_result['#status'])) {
          drupal_add_http_header('status', $page_callback_result['#status']);
        }
        if (!empty($page_callback_result['#headers'])) {
          foreach ($page_callback_result['#headers'] as $header => $val) {
            drupal_add_http_header($header, $val);
          }
        }
        if (!empty($page_callback_result['#content'])) {
          print $page_callback_result['#content'];
        }
      }
      break;
  }
}

/**
 * Get a list of remotes.
 */
function party_service_remote_options_list($element, $name = '') {
  return db_select('party_service_remote', 'r')
    ->fields('r', array('name', 'label'))
    ->execute()
    ->fetchAllKeyed();
}